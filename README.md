# Family quiz

## Slide show

Quiz slide show deck created with [Marpit](https://marpit.marp.app/).

Edit `slide-show.md`, then export to slide deck via Marpit CLI or VS Code tool.

Speaker notes in `questions-and-answers.md`.

## Web pages

Hosted: https://tobz1000.gitlab.io/quiz-2020-05-18/

Source under `webpage`, built with Jekyll, deployed with gitlab-ci. 