1. Riddle: What belongs to you, but other people use it more than you?
- Your name

2. Anagram of 2009 film set in WW2: "Long Sour Absurdities"
- Inglourious Basterds

3. What is the name of the first computer program to beat a world champion at the board game Go in 2017?
- AlphaGo Master (accept AlphaGo)

4. What was the highest value ever reached by Bitcoin, in pounds at the time, to the nearest 1000? Was in December 2017.
- £14,588 (accept £14,000 - £15,000)

5. A tesseract is a 4-dimensional shape which is the extension of a cube. How many points/corners does a tesseract have?
- 16

6. In which city was the first man-made nuclear reaction observed in 1917?
- Manchester
- Ernest Rutherford, UoM, Nitrogen -> Oxygen

7. (2pt) Where can you find Stilling's Canal? Up to 2 points, depending on how precise you are
- In the eye
- 1 point for the body or any body part; 2 points for eye

8. Which English monarch was crowned on my birthday, 30 October, in 1485?
- Henry VII
- 1/2 pt for any Henry

9. (2pt) "Pykrete" is a composite from which the UK hoped to construct an aircraft carrier in WW2. What two materials is it made from? 1 point for each material.
- Ice
- Sawdust (accept wood pulp)

10. Roughly how tall are the Pillars of Creation?:
    a) 10 nanometres
    b) 100 metres
    c) 100,000 km
    d) 10 light years
- d) 10 light years

Score out of 12
