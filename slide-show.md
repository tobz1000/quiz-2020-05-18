---
marp: true
---

<!-- theme: gaia -->
<!-- class: invert -->

1) What belongs to you, but other people use it more than you?

2) 2009 film set in WW2: "Long Sour Absurdities"

3) What was the first computer program to beat a world champion at Go in 2017?

4) What was the highest value of 1 Bitcoin? (nearest £1000, Dec 2017)

5) How many points/corners are in a tesseract?

---

6) In which city was the first man-made nuclear reaction in 1917?

7) Where can you find Stilling's Canal? (2 points)

8) Which English monarch was crowned on 30 October 1485?

9) What two materials is Pykrete made from? (2 points)

10) What is the rough height of the Pillars of Creation?
    - 10 nanometres
    - 100 metres
    - 100,000 km
    - 10 light years

---

## Answers

1. What belongs to you, but other people use it more than you?
    * Your name

2. "Long Sour Absurdities"
    * Inglourious Basterds

3. First computer program to beat a world champion at Go?
    * AlphaGo Master (accept AlphaGo)

---

## Answers

<style scoped>
/* Absolute positioning allows the image to be a bit bigger */
img {
    position: absolute;
    bottom: 20px;
    right: 20px;
}
</style>

4. Highest value reached by Bitcoin?
    * £14,588 (£14,000 or £15,000)

5. How many points/corners in a tesseract?
    * 16<br>![height:450px](images/tesseract.png)

---

## Answers

6. City of first man-made nuclear reaction? (1917)
    * Manchester

7. Where can you find Stilling's Canal? (2 points)
    * In the eye<br>![height:300px](images/stillings-canal.png)

---

## Answers

8. English monarch crowned on 30 October 1485?
    * Henry VII

9. Two materials Pykrete is made from? (2 points)
    * Ice & Sawdust

---

## Answers

10. Pillars of Creation height?
    * 10 light years<br>![bg](images/pillars-of-creation.jpg)