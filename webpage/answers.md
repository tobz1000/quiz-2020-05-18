---
layout: page
title: Answers
---

1. Riddle: What belongs to you, but other people use it more than you?
- Your name (1 point)

2. Anagram of 2009 film set in World War II: "Long Sour Absurdities"
- Inglourious Basterds (1 point)

3. What is the name of the first computer program to beat a world champion at the board game "Go" in 2017?
- [AlphaGo](https://en.wikipedia.org/wiki/AlphaGo) (1 point)
- Technically, it was the later version "AlphaGo Master" (either answer is fine).

4. Bitcoin reached its highest ever value in December 2017. What was the value of 1 Bitcoin at its peak?
- £14,588 (1 point)
- Any answer from £14,000 to £15,000 is acceptable

5. A tesseract is a 4-dimensional shape, and is the 4D extension of a cube. How many points/corners does a tesseract have?
- 16 (1 point)
- Below is a "view" of a tesseract in 3D space. The two cubes could be considered the "front" and "back" of the shape, much like squares in a cube:

    ![](images/tesseract.png){:width="300px"}

6. In which city was the first man-made nuclear reaction observed in 1917?
- Manchester (1 point)
- This was at the University of Manchester, [performed by Ernest Rutherford](https://en.wikipedia.org/wiki/Nuclear_fission#History). It was the conversion of nitrogen into oxygen.

7. Where can you find Stilling's Canal?
- [In the eye](https://en.wikipedia.org/wiki/Hyaloid_canal) (2 points)
- 1 point for "in the body", or for mentioning any other body part
- More commonly known as the Hyaloid Canal

    ![](images/stillings-canal.png){:width="300px"}

8. My birthday is 30 October. Which English monarch was crowned on 30 October, in 1485?
- Henry VII (1 point)
- Half a point if you mention a Henry with the wrong number.

9. "Pykrete" is a composite material with which the UK hoped to construct an aircraft carrier in World War II. What two ordinary materials is Pykrete made from?
- Ice and Sawdust (2 points)
- One point for each material

10. Roughly how tall are the Pillars of Creation?:
- d) 10 light years (1 point)
- Half a point for the second-closest answer: c) 100,000 km
- This is the name given to 3 huge pillars of interstellar gas photographed by the Hubble Space Telescope:

    ![](images/pillars-of-creation.jpg)

Total score is out of 12.