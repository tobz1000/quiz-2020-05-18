---
layout: page
title: Questions
---

1. Riddle: What belongs to you, but other people use it more than you?

2. Anagram of 2009 film set in World War II: "Long Sour Absurdities"

3. What is the name of the first computer program to beat a world champion at the board game "Go" in 2017?

4. Bitcoin reached its highest ever value in December 2017. What was the value of 1 Bitcoin, at its peak, to the nearest £1000?

5. A tesseract is a 4-dimensional shape, and is the 4D extension of a cube. How many points/corners does a tesseract have?

6. In which city was the first man-made nuclear reaction observed in 1917?

7. Where can you find Stilling's Canal?
- Up to two points - depending on how precise you are.

8. My birthday is 30 October. Which English monarch was crowned on 30 October, in 1485?
- Half a point if you can get the name; a whole point for the name and number.

9. "Pykrete" is a composite material with which the UK hoped to construct an aircraft carrier in World War II. What two ordinary materials is Pykrete made from?
- Up to two points - one for each material.

10. Roughly how tall are the Pillars of Creation?:
- a) 10 nanometres
- b) 100 metres
- c) 100,000 km
- d) 10 light years

Go to the [Answers](answers) when you're ready...